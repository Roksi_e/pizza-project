window.addEventListener("DOMContentLoaded", () => {});
const [...radioBtn] = document.querySelectorAll(".radio > input"),
  [...resultAll] = document.querySelectorAll(".result > div"),
  pissaTable = document.querySelector(".table"),
  [...ingridients] = document.querySelectorAll(".ingridients > div > img"),
  pissaPrice = {
    small: "50",
    mid: "60",
    big: "90",
    // cheese : '50',
    // feta : '60',
    // mocarela : '70',
    // beef : '80',
    // tomato : '50',
    // mushrooms : '60'
  };

let img = "",
  price = 0,
  priceAll = 0,
  itemPissaPrice = 0,
  sauceClassicPrice = 0,
  sauceBBQPrice = 0,
  sauceRikottaPrice = 0;

resultAll.map((item) => {
  const div = createElement("div", "result-div");
  return item.appendChild(div);
});

radioBtn.map((item) => {
  item.addEventListener("click", (event) => {
    if (event.target.value === "small") {
      price = pissaPrice.small;
    }
    if (event.target.value === "mid") {
      price = pissaPrice.mid;
    }
    if (event.target.value === "big") {
      price = pissaPrice.big;
    }
    return (resultAll[0].lastChild.textContent = `${price} грн`);
  });
});

ingridients.forEach((elem) => {
  elem.addEventListener(
    "dragstart",
    function (evt) {
      evt.dataTransfer.effectAllowed = "move";
      evt.dataTransfer.setData("Text", this.id);
    },
    false
  );
});

pissaTable.addEventListener(
  "dragover",
  function (evt) {
    if (evt.preventDefault) evt.preventDefault();
    return false;
  },
  false
);

pissaTable.addEventListener(
  "drop",
  function (evt) {
    if (evt.preventDefault) evt.preventDefault();
    if (evt.stopPropagation) evt.stopPropagation();
    let id = evt.dataTransfer.getData("Text");
    let elem = document.getElementById(id);
    if (elem.id === "sauceClassic") {
      elements[0].counter++;
      img = createElement(
        "img",
        "yellow-elemnt",
        undefined,
        "Pizza_pictures/sous-klassicheskij_1557758736353.png"
      );
      resultAll[1].lastElementChild.append(elements[0].div2);
      elements[0].div2.append(elements[0].div3);
      elements[0].button1.textContent = `${elements[0].counter}`;
      elements[0].div3.append(elements[0].button1);
      elements[0].div3.append(elements[0].button2);
      sauceClassicPrice =
        elements[0].counter * parseInt(elements[0].pissaPrice);
    }
    if (elem.id === "sauceBBQ") {
      elements[1].counter++;
      img = createElement(
        "img",
        "yellow-elemnt",
        undefined,
        "Pizza_pictures/sous-bbq_155679418013.png"
      );
      resultAll[1].lastElementChild.append(elements[1].div2);
      elements[1].div2.append(elements[1].div3);
      elements[1].button1.textContent = `${elements[1].counter}`;
      elements[1].div3.append(elements[1].button1);
      elements[1].div3.append(elements[1].button2);
      sauceBBQPrice = elements[1].counter * parseInt(elements[1].pissaPrice);
    }
    if (elem.id === "sauceRikotta") {
      elements[2].counter++;
      img = createElement(
        "img",
        "yellow-elemnt",
        undefined,
        "Pizza_pictures/sous-rikotta_1556623391103.png"
      );
      resultAll[1].lastElementChild.append(elements[2].div2);
      elements[2].div2.append(elements[2].div3);
      elements[2].button1.textContent = `${elements[2].counter}`;
      elements[2].div3.append(elements[2].button1);
      elements[2].div3.append(elements[2].button2);
      sauceRikottaPrice =
        elements[2].counter * parseInt(elements[2].pissaPrice);
    }
    priceAll =
      parseInt(price) + sauceClassicPrice + sauceBBQPrice + sauceRikottaPrice;
    this.appendChild(img);
    resultAll[0].lastChild.textContent = `${priceAll} грн`;

    return false;
  },
  false
);
