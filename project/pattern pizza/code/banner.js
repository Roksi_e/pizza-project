const main = document.querySelector("main"),
  banner = document.querySelector("#banner");

const random = (min, max) => {
  return Math.floor(Math.random() * (max - min + 1) + min);
};

banner.addEventListener("mouseover", (event) => {
  const { width, height } = main.getBoundingClientRect(),
    bannerStyle = getComputedStyle(banner);

  banner.style.bottom = `${random(
    0,
    height - parseFloat(bannerStyle.height)
  )}px`;
  banner.style.right = `${random(0, width - parseFloat(bannerStyle.width))}px`;
});
